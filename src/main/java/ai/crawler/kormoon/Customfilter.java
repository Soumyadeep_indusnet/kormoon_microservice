package ai.crawler.kormoon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import ai.crawler.kormoon.dto.request.UserDTO;

public class Customfilter extends OncePerRequestFilter {

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	ModelMapper mapper;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		//logger.info(request.getHeader("Authorization").toString());
		
		HttpHeaders headers = new HttpHeaders();
	    headers.add("Authorization", request.getHeader("Authorization"));
		//headers.add("Authorization","Bearer b2b3cdb4-b1c5-406d-b6e3-2d360e3e3455"););
		
	    // create request
	    HttpEntity request1 = new HttpEntity(headers);
	    
		ResponseEntity<Object> data =
				restTemplate.exchange("http://kormoon-user-service/getUserInfo",HttpMethod.GET, request1, Object.class);
		//logger.info(data.getBody());
		UserDTO user = mapper.map(data.getBody(), UserDTO.class);
		
		List<String> auhorities = user.getAuthorities();
		
		List<SimpleGrantedAuthority> updatedAuthorities = new ArrayList<SimpleGrantedAuthority>();
		updatedAuthorities.addAll( 
					auhorities.stream()
					.map(authority -> new SimpleGrantedAuthority(authority))
					.collect(Collectors.toList())
				);
		SecurityContextHolder.getContext().setAuthentication(
		        new UsernamePasswordAuthenticationToken(
		        		user,
		                null,
		                updatedAuthorities)
		);
		
		filterChain.doFilter(request, response);
	}
	
}
