package ai.crawler.kormoon.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ai.crawler.kormoon.dto.request.CrawlerCreateRequestDTO;
import ai.crawler.kormoon.dto.request.UserDTO;
import ai.crawler.kormoon.dto.response.CrawlerListResponse;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.dto.response.SuccessResponse;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.service.CrawlerListService;
import ai.crawler.kormoon.utils.utils2;


@CrossOrigin
@RestController
public class CrawlerController {
	
	@Autowired
	CrawlerListService crawlerListService;
	
	@Autowired
	MessageSource messageSource;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	public utils2 util;
	
	@GetMapping("/crawlerList")
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
	
	
	ListResponse<CrawlerListResponse> findCrawlerDataList(
			
			@RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "search", required = false, defaultValue = "") String search,
			@RequestParam(value = "searchByStatus", required = false, defaultValue = "") String searchByStatus,
			@RequestParam(value = "sortBy", required = false, defaultValue = "") String sortBy,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "DESC") String sortOrder) throws DataInvalidException {
		
		util.meth1();
		return crawlerListService.findCrawlerDataList(pageNo, pageSize, search, searchByStatus, sortBy, sortOrder);
		
	}
	
	@GetMapping(value = "/crawlerById", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
	public Optional<SettingsEntity> getCrawlerById(@RequestParam Long crawlerId) throws DataInvalidException {
		
		util.meth1();
		return crawlerListService.getAllListByCrawlerId(crawlerId);

	}
	

	@PutMapping(value = "crawlerUpdate/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SuccessResponse> updateCrawlerById(@PathVariable(value = "id") Long id,
    		 @RequestBody CrawlerCreateRequestDTO crawlerCreateRequestDTO) throws DataInvalidException{
		
		crawlerListService.updateCrawlerById(id, crawlerCreateRequestDTO);
		util.meth1();
		return new ResponseEntity<>(new SuccessResponse(messageSource.getMessage("app.update.success", null, null)),
				HttpStatus.OK);
    }
	
}
