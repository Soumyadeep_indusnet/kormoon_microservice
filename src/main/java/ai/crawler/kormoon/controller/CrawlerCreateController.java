package ai.crawler.kormoon.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import ai.crawler.kormoon.dto.request.CrawlerCreateRequestDTO;
import ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO;
import ai.crawler.kormoon.dto.response.SectorResponseDTO;
import ai.crawler.kormoon.dto.response.SuccessResponse;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.service.CrawlerCreateService;
import ai.crawler.kormoon.utils.utils2;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@Slf4j



@RequestMapping("crawler-create")
public class CrawlerCreateController {

	@Autowired
	CrawlerCreateService crawlerCreateService;

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	public utils2 util;
	
	
	@GetMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
	public List<BasicIdAndNameResponseDTO> getAllCountriesList() {
		
		log.info("Get countries list started");
		util.meth1();
		return crawlerCreateService.getAllCountriesList();
		
	}

	@GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public List<BasicIdAndNameResponseDTO> getAllCategoriesList() {
		util.meth1();
		log.info("Get categories list started");

		return crawlerCreateService.getAllCategoriesList();

	}

	@GetMapping(value = "/sub-categories", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public List<BasicIdAndNameResponseDTO> getAllSubCategoriesListByCategoryId(@RequestParam Long categoryId) {
		util.meth1();
		log.info("Get sub categories list by category id started");

		return crawlerCreateService.getAllSubCategoriesListByCategoryId(categoryId);

	}

	@GetMapping(value = "/sectors", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public List<SectorResponseDTO> getAllSectorsList() {
		util.meth1();
		log.info("Get sectors list started");

		return crawlerCreateService.getAllSectors();

	}

	@PostMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public ResponseEntity<SuccessResponse> save(@Valid @RequestBody CrawlerCreateRequestDTO request) throws DataInvalidException {
		util.meth1();
		log.info("Create crawler started");

		crawlerCreateService.save(request);

		log.info("Create crawler ended"); 

		return new ResponseEntity<>(new SuccessResponse(messageSource.getMessage("app.save.success", null, null)),
				HttpStatus.OK);

	}

}
