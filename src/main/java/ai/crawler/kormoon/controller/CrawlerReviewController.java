package ai.crawler.kormoon.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.crawler.kormoon.dto.request.ContentRequestDTO;
import ai.crawler.kormoon.dto.request.IgnoreChildUrlRequestDTO;
import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerReviewResponseDTO;
import ai.crawler.kormoon.dto.response.SuccessResponse;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.service.CrawlerReviewService;
import ai.crawler.kormoon.utils.utils2;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@Slf4j
@RequestMapping("crawler-review")
public class CrawlerReviewController {

	@Autowired
	CrawlerReviewService crawlerReviewService;

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	public utils2 util;

	@GetMapping(path = "/crawled-urls", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public CrawlerReviewResponseDTO getCrawledUrls(@RequestParam Long settingsId,
			@RequestParam(value = "pageNo", required = false, defaultValue = "0") Integer pageNo,
			@RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit,
			@RequestParam(value = "sortBy", required = false, defaultValue = "createdDate") String sortBy,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "DESC") String sortOrder)
			throws DataInvalidException {
		util.meth1();
		log.info("Get crawled urls by settings id started");

		return crawlerReviewService.getCrawledUrls(settingsId, pageNo, limit, sortBy, sortOrder);

	}

	@PutMapping(value = "/ignore-url", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public ResponseEntity<SuccessResponse> ignoreUrl(@Valid @RequestBody IgnoreChildUrlRequestDTO request)
			throws DataInvalidException {
		util.meth1();
		log.info("Ignore/ Include child url started");

		crawlerReviewService.ignoreUrl(request);

		log.info("Ignore/ Include child url ended");

		return new ResponseEntity<>(new SuccessResponse(messageSource.getMessage("app.save.success", null, null)),
				HttpStatus.OK);

	}

	@GetMapping(path = "/get-content/{childId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public ContentResponseDTO getContent(@PathVariable Long childId) throws DataInvalidException {
		util.meth1();
		log.info("Get content by child id started");

		return crawlerReviewService.getContent(childId);

	}

	@PutMapping(value = "/edit-content", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	public ResponseEntity<SuccessResponse> editContent(@Valid @RequestBody ContentRequestDTO request)
			throws DataInvalidException {
		util.meth1();
		log.info("Edit content started");

		crawlerReviewService.editContent(request);

		log.info("Edit content child url ended");

		return new ResponseEntity<>(new SuccessResponse(messageSource.getMessage("app.update.success", null, null)),
				HttpStatus.OK);

	}
	
}
