package ai.crawler.kormoon.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.crawler.kormoon.dto.request.IgnoreChildUrlRequestDTO;
import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerReviewResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerSearchListResponseDTO;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.dto.response.SuccessResponse;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.service.CrawlerSearchService;
import ai.crawler.kormoon.utils.utils2;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin
@RestController
@Slf4j
@RequestMapping("crawler-search")
public class CrawlerSearchController {

	@Autowired
	CrawlerSearchService crawlerSearchService;

	@Autowired
	MessageSource messageSource;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	public utils2 util;

	@GetMapping("/crawler-list")
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	ListResponse<CrawlerSearchListResponseDTO> findCrawlerDataList(
			@RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "10") int pageSize,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword,
			@RequestParam(value = "sortBy", required = false, defaultValue = "createdDate") String sortBy,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "DESC") String sortOrder)
			throws DataInvalidException {
		util.meth1();
		log.info("Get crawler list for search started");

		return crawlerSearchService.getCrawlerList(pageNo, pageSize, keyword, sortBy, sortOrder);

	}

	@GetMapping(path = "/crawled-urls", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	CrawlerReviewResponseDTO getCrawledUrls(@RequestParam Long settingsId,
			@RequestParam(value = "pageNo", required = false, defaultValue = "0") Integer pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "50") Integer pageSize,
			@RequestParam(value = "sortBy", required = false, defaultValue = "createdDate") String sortBy,
			@RequestParam(value = "sortOrder", required = false, defaultValue = "DESC") String sortOrder,
			@RequestParam(value = "keyword", required = false, defaultValue = "") String keyword)
			throws DataInvalidException {
		util.meth1();
		log.info("Get crawled urls by settings id and keyword started");

		return crawlerSearchService.getCrawledUrls(settingsId, pageNo, pageSize, sortBy, sortOrder, keyword);

	}

	@PutMapping(value = "/ignore-url", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	ResponseEntity<SuccessResponse> ignoreUrl(@Valid @RequestBody IgnoreChildUrlRequestDTO request)
			throws DataInvalidException {
		util.meth1();
		log.info("Ignore/ Include child url started");

		crawlerSearchService.ignoreUrl(request);

		log.info("Ignore/ Include child url ended");

		return new ResponseEntity<>(new SuccessResponse(messageSource.getMessage("app.save.success", null, null)),
				HttpStatus.OK);

	}

	@GetMapping(path = "/get-content/{childId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasAnyRole('ROLE_SUPER_ADMIN','ROLE_ADMIN','ROLE_USER')")
	ContentResponseDTO getContent(@PathVariable Long childId) throws DataInvalidException {
		util.meth1();
		log.info("Get content by child id started");

		return crawlerSearchService.getContent(childId);

	}
}
