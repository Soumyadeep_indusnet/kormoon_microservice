package ai.crawler.kormoon.controller;

import javax.servlet.http.HttpServletRequest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import ai.crawler.kormoon.dto.request.UserDTO;

@RestController
@CrossOrigin
public class TestController {

//	@Autowired
//	RestTemplate restTemplate;
	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private HttpServletRequest request;
	
	@GetMapping(value = "/test")
	@PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
	public UserDTO demo() throws Exception {
		
//		System.out.println("hello");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		UserDTO userDto = (UserDTO) auth.getPrincipal();
		
		return userDto;
	}
}
