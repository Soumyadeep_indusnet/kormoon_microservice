package ai.crawler.kormoon.dto.request;

import lombok.Data;

@Data
public class ContentRequestDTO {

	Long childId;

	String content;

}
