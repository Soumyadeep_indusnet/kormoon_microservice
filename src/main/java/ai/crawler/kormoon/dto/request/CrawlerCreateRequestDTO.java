package ai.crawler.kormoon.dto.request;

import java.util.Date;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.drew.lang.annotations.NotNull;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CrawlerCreateRequestDTO {
	
	@NotBlank
	String name;
	
	@NotBlank
	String url;
	
	@NotEmpty
	Set<String> keywordList;
	
	@NotNull
	@Min(1)
	@Max(5)
	Integer maxDepthLevel;
	
	@NotNull
	Date startsFrom;
	
	@NotBlank
	String frequency;
	
	@NotNull
	Boolean isSecuredURL;
	
	String loginUsername;
	
	String loginPassword;
	
	@NotNull
	Boolean failureAlert;
	
	@NotNull
	String status;
	
	@NotEmpty
	Set<Long> selectedCountryIdList;
	
	@NotNull
	Long sectorId;
	
	@NotNull
	Long sourceSubCategoryId;

}
