package ai.crawler.kormoon.dto.request;

import lombok.Data;

@Data
public class IgnoreChildUrlRequestDTO {
	
	Long childId;
	
	Boolean isIgnored;

}
