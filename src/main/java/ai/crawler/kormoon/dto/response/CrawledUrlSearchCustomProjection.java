package ai.crawler.kormoon.dto.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CrawledUrlSearchCustomProjection {
	
	Long id;
	
	String keyword;

	String url;

	String content;
	
	Boolean isIgnored;

}
