package ai.crawler.kormoon.dto.response;

import java.util.HashSet;
import java.util.Set;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CrawledUrlsResponseDTO {

	Long id;
	
	Set<String> keywords = new HashSet<>();

	String url;

	String content;
	
	Boolean isIgnored;

}
