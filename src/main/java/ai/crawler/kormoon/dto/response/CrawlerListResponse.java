package ai.crawler.kormoon.dto.response;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;

import ai.crawler.kormoon.entity.CountryEntity;
import ai.crawler.kormoon.entity.SectorEntity;
import ai.crawler.kormoon.entity.SourceSubCategoryEntity;
import ai.crawler.kormoon.enums.FrequencyType;
import ai.crawler.kormoon.enums.SettingsStateType;
import ai.crawler.kormoon.enums.SettingsStatusType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CrawlerListResponse {

	Long id;
	
	String name;
	
	String url;
	
	Set<String> keywordList = new HashSet<>();
	
	Integer maxDepthLevel;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	Date lastRunOn;
	
	@JsonFormat(pattern = "dd-MM-yyyy")
	Date nextScheduleOn;
	
	FrequencyType frequency;
	
	Boolean isSecuredURL;
	
	String loginUsername;
	
	String loginPassword;
	
	String failureAlert;
	
	SettingsStatusType status;
	
	SettingsStateType state;
	
	Set<CountryEntity> selectedCountriesList = new HashSet<>();
	
	SectorEntity sector;
	
	SourceSubCategoryEntity sourceSubCategory;
	
}
