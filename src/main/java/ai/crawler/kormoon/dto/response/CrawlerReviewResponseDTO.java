package ai.crawler.kormoon.dto.response;

import java.util.HashSet;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CrawlerReviewResponseDTO {
	
	String crawlerName;
	
	String url;
	
	Set<String> keywords = new HashSet<>();
	
	ListResponse<CrawledUrlsResponseDTO> crawledUrls = new ListResponse<>();

}
