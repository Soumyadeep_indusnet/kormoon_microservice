package ai.crawler.kormoon.dto.response;

import java.util.HashSet;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CrawlerSearchListResponseDTO {
	
	Long id;

	String name;

	String url;

	Set<String> keywordList = new HashSet<>();

}
