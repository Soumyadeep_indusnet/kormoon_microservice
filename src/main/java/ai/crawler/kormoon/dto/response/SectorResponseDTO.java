package ai.crawler.kormoon.dto.response;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SectorResponseDTO {

	Sector sector;

	@Data
	@FieldDefaults(level = AccessLevel.PRIVATE)
	public static class Sector {

		Long id;

		String name;

		List<SectorResponseDTO> childSectorsList = new ArrayList<>();

	}

}
