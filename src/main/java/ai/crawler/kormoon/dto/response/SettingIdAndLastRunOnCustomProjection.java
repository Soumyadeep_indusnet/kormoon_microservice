package ai.crawler.kormoon.dto.response;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SettingIdAndLastRunOnCustomProjection {
	
	Long settingsId;
	
	Date lastRunOn;

}
