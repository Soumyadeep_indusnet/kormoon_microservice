package ai.crawler.kormoon.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name="tbl_crawled_data")
@Data
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class CrawledDataEntity extends BaseEntity {

	static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	Long id;
	
	@Column(name="url", columnDefinition = "TEXT")
	String url;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "tbl_crawled_data_keyword", joinColumns = @JoinColumn(name = "crawled_data_id"))
    @Column(name = "keyword")
	Set<String> keywordList = new HashSet<>();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="settings_id")
	SettingsEntity ruleEntity;
}
