package ai.crawler.kormoon.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ai.crawler.kormoon.enums.FrequencyType;
import ai.crawler.kormoon.enums.RuleStateType;
import ai.crawler.kormoon.enums.RuleStatusType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name="tbl_rule")
@Data
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class RuleEntity extends BaseEntity{

	static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	Long id;
	
	@Column(name="name", columnDefinition = "TEXT")
	String name;
	
	@Column(name="url", columnDefinition = "TEXT")
	String url;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "tbl_rule_keyword", joinColumns = @JoinColumn(name = "rule_id"))
    @Column(name = "keyword")
	Set<String> keywordList = new HashSet<>();
	
	@Column(name = "max_depth_level")
	Integer maxDepthLevel;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "starts_from")
	Date startsFrom;
	
	@Column(name = "frequency")
	@Enumerated(EnumType.STRING)
	FrequencyType frequency;
	
	@Column(name = "is_secured", columnDefinition = "tinyint(1) default 0")
	Boolean isSecuredURL;
	
	@Column(name = "login_user_name")
	String loginUserName;
	
	@Column(name = "login_password")
	String loginPassword;
	
	@Column(name = "failure_alert", columnDefinition = "tinyint(1) default 0")
	Boolean failureAlert;
	
	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	RuleStatusType status;
	
	@Column(name = "state")
	@Enumerated(EnumType.STRING)
	RuleStateType state;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "tbl_rule_country", joinColumns = @JoinColumn(name = "rule_id"), inverseJoinColumns = @JoinColumn(name = "country_id"))
	Set<CountryEntity> selectedCountriesList = new HashSet<>();
	
	@ManyToOne
	@JoinColumn(name="sector_id")
	SectorEntity sector;
	
	@ManyToOne
	@JoinColumn(name="source_sub_category_id")
	SourceSubCategoryEntity sourceSubCategory;
	
	
}
