package ai.crawler.kormoon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Table(name="tbl_sector")
@Data
@NoArgsConstructor
@FieldDefaults(level=AccessLevel.PRIVATE)
public class SectorEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	Long id;
	
	@Column(name="name")
	String name;
	
	@Column(name="level")
	Integer level;
	
	@ManyToOne
	@JoinColumn(name="parent_sector_id")
	SectorEntity parentSector;
	
}
