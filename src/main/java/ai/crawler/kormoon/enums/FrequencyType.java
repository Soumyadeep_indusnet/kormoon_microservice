package ai.crawler.kormoon.enums;

public enum FrequencyType {

	DAILY,
	WEEKLY,
	BI_WEEKLY
	
}
