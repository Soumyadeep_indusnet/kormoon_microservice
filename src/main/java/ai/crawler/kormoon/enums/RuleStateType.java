package ai.crawler.kormoon.enums;

public enum RuleStateType {

	NOT_YET_STARTED,
	RUNNING,
	COMPLETED
	
}
