package ai.crawler.kormoon.enums;

public enum SettingsStateType {

	NOT_YET_STARTED,
	RUNNING,
	COMPLETED
	
}
