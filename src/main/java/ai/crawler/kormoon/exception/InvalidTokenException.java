package ai.crawler.kormoon.exception;

import org.springframework.security.oauth2.client.ClientAuthorizationRequiredException;

public class InvalidTokenException extends ClientAuthorizationRequiredException{

	public InvalidTokenException(String msg) {
		super(msg);
		
	}
	 private static final long serialVersionUID = 1L;
}
