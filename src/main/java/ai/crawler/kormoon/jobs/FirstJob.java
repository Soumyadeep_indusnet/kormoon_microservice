package ai.crawler.kormoon.jobs;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.repository.SettingsRepository;
import ai.crawler.kormoon.service.CrawledDataService;

@Component
public class FirstJob {

	@Autowired
	CrawledDataService crawledDataService;
	
	@Autowired
	SettingsRepository ruleRepository;
	
//	@Scheduled(cron = "${firstjob.cron}")
	@Scheduled(fixedRate = 100000000)
	@Transactional
	private void run() {
		
		Date d1 = new Date();
		
		List<SettingsEntity> ruleList =  ruleRepository.findAll();
		System.out.println("rulelis"+ruleList.size());
		System.out.println("rulelis"+ruleList);
		for(SettingsEntity ruleEntity : ruleList) {
			
			try {
				
				crawledDataService.getPageLinks(ruleEntity.getUrl(), ruleEntity.getKeywordList(), ruleEntity.getMaxDepthLevel(), ruleEntity);
				
			} catch(Exception e) {
				
			}
			
		}
		
		Date d2 = new Date();
		System.out.println("Total time taken : " + (d2.getTime() - d1.getTime())/1000);
		
	}
	
}
