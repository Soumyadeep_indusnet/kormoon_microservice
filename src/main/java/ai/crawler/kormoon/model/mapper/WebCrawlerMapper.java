package ai.crawler.kormoon.model.mapper;

import java.util.Date;

import ai.crawler.kormoon.dto.response.CrawlerListResponse;
import ai.crawler.kormoon.entity.SettingsEntity;

public class WebCrawlerMapper {

	public static CrawlerListResponse toCrawlerListResponse(SettingsEntity ruleEntity, Date lastRunOn,
			Date nextScheduledOn) {

		return CrawlerListResponse.builder()
				.id(ruleEntity.getId())
				.name(ruleEntity.getName())
				.url(ruleEntity.getUrl())
				.keywordList(ruleEntity.getKeywordList())
				.maxDepthLevel(ruleEntity.getMaxDepthLevel())
				.nextScheduleOn(nextScheduledOn)
				.lastRunOn(lastRunOn)
				.frequency(ruleEntity.getFrequency())
				.isSecuredURL(ruleEntity.getIsSecuredURL())
				.loginPassword(ruleEntity.getLoginPassword())
				.loginPassword(ruleEntity.getLoginPassword())
				.failureAlert(ruleEntity.getFailureAlert() ? "Enabled" : "Disabled")
				.status(ruleEntity.getStatus())
				.state(ruleEntity.getState())
				.selectedCountriesList(ruleEntity.getSelectedCountriesList())
				.sector(ruleEntity.getSector())
				.sourceSubCategory(ruleEntity.getSourceSubCategory()).build();

	}

}
