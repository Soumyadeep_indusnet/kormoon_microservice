package ai.crawler.kormoon.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO;
import ai.crawler.kormoon.entity.CountryEntity;

public interface CountryRepository extends JpaRepository<CountryEntity, Long> {
	
	@Query(value =    " SELECT new ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO("
								+ " country.id,"
								+ " country.name"
								+ " )"
								+ " FROM CountryEntity country"
								+ " ORDER BY country.name ASC")
	List<BasicIdAndNameResponseDTO> getAllCountriesListOrderByNameAsc();

	@Query(value =    " SELECT country FROM CountryEntity country"
								+ " WHERE country.id IN (?1)") 
	Set<CountryEntity> findAllByIdIn(Set<Long> countryIdList);
	
}
