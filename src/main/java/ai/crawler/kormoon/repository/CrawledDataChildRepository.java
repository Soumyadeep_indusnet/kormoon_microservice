package ai.crawler.kormoon.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawledUrlSearchCustomProjection;
import ai.crawler.kormoon.entity.CrawledDataChildEntity;
import ai.crawler.kormoon.entity.CrawledDataEntity;

@Repository
public interface CrawledDataChildRepository extends JpaRepository<CrawledDataChildEntity, Long> {
	Optional<CrawledDataChildEntity> findByCrawledDataEntityAndUrl(CrawledDataEntity crawledDataEntity, String url);

	@Query("SELECT coalesce(max(cde.level), 0) FROM CrawledDataChildEntity cde " + "WHERE cde.url = ?1 "
			+ "AND cde.crawledDataEntity = ?2")
	Long getMaxId(String url, CrawledDataEntity crawledDataEntity);

	List<CrawledDataChildEntity> findByCrawledDataEntityAndIsFetchedOrderByIdAsc(CrawledDataEntity crawledDataEntity,
			Long isFetched);
	
	  @Query(value = " SELECT child FROM CrawledDataChildEntity child" 
							  +	  " WHERE child.crawledDataEntity.ruleEntity.id LIKE ?1") 
		Page<CrawledDataChildEntity> findAllBySettingsId(Long settingsId, Pageable pageable);

	 @Query(value =  " SELECT new ai.crawler.kormoon.dto.response.ContentResponseDTO("
						 		+ " child.content"
						 		+ " )"
						 		+ " FROM CrawledDataChildEntity child"
						 		+ " WHERE child.id LIKE ?1")
	 ContentResponseDTO getContentByChildId(Long childId);
	 
	 @Query(value = " SELECT child.url FROM CrawledDataChildEntity child" 
			  +	  " WHERE child.crawledDataEntity.ruleEntity.id LIKE ?1"
			  +   " AND child.isIgnored = true") 
	 Set<String> findIgnoreUrlsBySettingsId(Long settingsId);

	 @Query(value = " SELECT child FROM CrawledDataChildEntity child" 
			  +	  " WHERE child.crawledDataEntity.ruleEntity.id LIKE ?1"
			  +   " AND child.url = ?2") 
	 Optional<CrawledDataChildEntity> findByUrlLike(Long settingsId, String url);
	 
	 @Query(value =   " SELECT new ai.crawler.kormoon.dto.response.CrawledUrlSearchCustomProjection("
						 		+ " child.id,"
						 		+ " keyword,"
						 		+ " child.url,"
						 		+ " SUBSTRING(child.content, 1, 100),"
						 		+ " child.isIgnored"
						 		+ " )"
						 		+ " FROM CrawledDataChildEntity child"
						 		+ " JOIN child.keywordList keyword" 
								+ " WHERE keyword LIKE %?1%"
								+ " AND child.crawledDataEntity.ruleEntity.id = ?2") 
		Page<CrawledUrlSearchCustomProjection> findAllBySettingsIdAndKeyword(String keyword, Long settingsId,
				Pageable pageable);
}
