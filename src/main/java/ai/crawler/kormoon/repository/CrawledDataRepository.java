package ai.crawler.kormoon.repository;

import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ai.crawler.kormoon.dto.response.SettingIdAndLastRunOnCustomProjection;
import ai.crawler.kormoon.entity.CrawledDataEntity;

@Repository
public interface CrawledDataRepository extends JpaRepository<CrawledDataEntity, Long> {
	Optional<CrawledDataEntity> findByUrlLike(String url);
	
	@Query(value =   " SELECT new ai.crawler.kormoon.dto.response.SettingIdAndLastRunOnCustomProjection("
								+ " crawledData.ruleEntity.id,"
								+ " MAX(crawledData.createdDate)"
								+ " )"
								+ " FROM CrawledDataEntity crawledData"
								+ " GROUP BY crawledData.ruleEntity")
	Set<SettingIdAndLastRunOnCustomProjection> getLastRunOnSet();

	@Query("SELECT count(c) from CrawledDataEntity c where c.ruleEntity.id = ?1")
	long countBySettingsId(Long settings_id);
}
