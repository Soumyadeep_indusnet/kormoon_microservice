package ai.crawler.kormoon.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ai.crawler.kormoon.entity.RuleEntity;

public interface RuleRepository extends JpaRepository<RuleEntity, Long>{

}
