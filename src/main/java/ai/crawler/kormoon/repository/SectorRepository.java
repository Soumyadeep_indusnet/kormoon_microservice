package ai.crawler.kormoon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ai.crawler.kormoon.entity.SectorEntity;

public interface SectorRepository extends JpaRepository<SectorEntity, Long> {
	
	List<SectorEntity> findAllByOrderByNameAsc();

}
