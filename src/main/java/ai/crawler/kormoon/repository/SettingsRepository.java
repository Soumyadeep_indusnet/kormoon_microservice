package ai.crawler.kormoon.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ai.crawler.kormoon.dto.response.CrawlerSearchListCustomProjection;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.enums.SettingsStatusType;

public interface SettingsRepository extends JpaRepository<SettingsEntity, Long>{
	
	@Query(value =    " SELECT CASE WHEN COUNT(rule) > 0"
					+ " THEN true ELSE false END"
					+ " FROM SettingsEntity rule"
					+ " WHERE LOWER(rule.name) LIKE LOWER(?1)")
	Boolean existsByName(String name);
	
	@Query(value =   "Select rule from SettingsEntity rule"
			+" where rule.name like %?1%"
			+" OR rule.frequency like %?1%")	
    Page<SettingsEntity> findByCrawlerName(String searchByNameFrequency, String searchByStatus, Pageable pageable);
	
	@Query(value =   "Select rule from SettingsEntity rule"
			+ " where (rule.name like %?1%"
			+ " OR rule.frequency like %?1%)"
			+ " AND (?2 IS NULL OR rule.status = ?2)")
	Page<SettingsEntity> findByCrawlerStatus(String searchByNameFrequency, String searchByStatus, Pageable pageable);
	
	@Query(value =   " SELECT new ai.crawler.kormoon.dto.response.CrawlerSearchListCustomProjection("
								+ " setting.id,"
								+ " setting.name,"
								+ " setting.url,"
								+ " keyword"
								+ " )"
								+ " FROM SettingsEntity setting"
								+ " JOIN setting.keywordList keyword"
								+ " WHERE keyword LIKE %?1%"
								+ " AND setting.status = ?2")
	Page<CrawlerSearchListCustomProjection> getCrawlerList(String keyword, SettingsStatusType activeStatus,
			Pageable pageable);

	Page<SettingsEntity> findAllByStatus(SettingsStatusType status, Pageable pageable);
	
}
