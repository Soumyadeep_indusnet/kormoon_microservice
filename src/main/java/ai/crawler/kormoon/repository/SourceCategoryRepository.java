package ai.crawler.kormoon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO;
import ai.crawler.kormoon.entity.SourceCategoryEntity;

public interface SourceCategoryRepository extends JpaRepository<SourceCategoryEntity, Long> {
	
	@Query(value =    " SELECT new ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO("
								+ " category.id,"
								+ " category.name"
								+ " )"
								+ " FROM SourceCategoryEntity category"
								+ " ORDER BY category.name ASC")
	List<BasicIdAndNameResponseDTO> getAllCategoriesListOrderByNameAsc();

}
