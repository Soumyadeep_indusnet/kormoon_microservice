package ai.crawler.kormoon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO;
import ai.crawler.kormoon.entity.SourceSubCategoryEntity;

public interface SourceSubCategoryRepository extends JpaRepository<SourceSubCategoryEntity, Long> {
	
	@Query(value =    " SELECT new ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO("
								+ " subcategory.id,"
								+ " subcategory.name"
								+ " )"
								+ " FROM SourceSubCategoryEntity subcategory"
								+ " WHERE subcategory.sourceCategory.id LIKE ?1"
								+ " ORDER BY subcategory.name ASC")
	List<BasicIdAndNameResponseDTO> getAllSubCategoriesListByCategoryIdOrderByNameAsc(Long categoryId);

}
