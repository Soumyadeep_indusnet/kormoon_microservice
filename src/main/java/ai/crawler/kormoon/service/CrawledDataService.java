package ai.crawler.kormoon.service;

import java.util.Set;

import ai.crawler.kormoon.entity.SettingsEntity;

public interface CrawledDataService {
	public void getPageLinks( String rootLink, Set<String> keyword, int i, SettingsEntity ruleEntity);
}
