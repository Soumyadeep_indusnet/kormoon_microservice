package ai.crawler.kormoon.service;

import java.util.List;

import ai.crawler.kormoon.dto.request.CrawlerCreateRequestDTO;
import ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO;
import ai.crawler.kormoon.dto.response.SectorResponseDTO;
import ai.crawler.kormoon.exception.DataInvalidException;

public interface CrawlerCreateService {
	
	List<BasicIdAndNameResponseDTO> getAllCountriesList();

	List<BasicIdAndNameResponseDTO> getAllCategoriesList();
	
	List<BasicIdAndNameResponseDTO> getAllSubCategoriesListByCategoryId(Long categoryId);
	
	List<SectorResponseDTO> getAllSectors();
	
	public void save(CrawlerCreateRequestDTO request) throws DataInvalidException;
	
}
