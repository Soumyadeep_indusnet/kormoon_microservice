package ai.crawler.kormoon.service;

import java.util.Optional;
import java.util.UUID;

import ai.crawler.kormoon.dto.request.CrawlerCreateRequestDTO;
import ai.crawler.kormoon.dto.response.CrawlerListResponse;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.exception.DataInvalidException;

public interface CrawlerListService {

	ListResponse<CrawlerListResponse> findCrawlerDataList(int pageNo, int pageSize, String searchByName,
			String searchByStatus, String sortBy, String sortOrder) throws DataInvalidException;
	
	Optional<SettingsEntity> getAllListByCrawlerId(Long crawlerId) throws DataInvalidException;
	
	public void updateCrawlerById(Long id,CrawlerCreateRequestDTO request) throws DataInvalidException;


}
