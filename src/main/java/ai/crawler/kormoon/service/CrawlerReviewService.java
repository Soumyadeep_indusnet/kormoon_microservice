package ai.crawler.kormoon.service;

import ai.crawler.kormoon.dto.request.ContentRequestDTO;
import ai.crawler.kormoon.dto.request.IgnoreChildUrlRequestDTO;
import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerReviewResponseDTO;
import ai.crawler.kormoon.exception.DataInvalidException;

public interface CrawlerReviewService {

	CrawlerReviewResponseDTO getCrawledUrls(Long settingsId, Integer pageNo, Integer limit, String sortBy,
			String sortOrder) throws DataInvalidException;

	public void ignoreUrl(IgnoreChildUrlRequestDTO request) throws DataInvalidException;

	public ContentResponseDTO getContent(Long childId) throws DataInvalidException;

	public void editContent(ContentRequestDTO request) throws DataInvalidException;

}
