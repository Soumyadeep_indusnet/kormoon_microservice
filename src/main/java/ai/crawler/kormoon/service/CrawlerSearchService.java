package ai.crawler.kormoon.service;

import ai.crawler.kormoon.dto.request.IgnoreChildUrlRequestDTO;
import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerReviewResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerSearchListResponseDTO;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.exception.DataInvalidException;

public interface CrawlerSearchService {

	ListResponse<CrawlerSearchListResponseDTO> getCrawlerList(Integer pageNo, Integer pageSize, String keyword,
			String sortBy, String sortOrder) throws DataInvalidException;

	CrawlerReviewResponseDTO getCrawledUrls(Long settingsId, Integer pageNo, Integer limit, String sortBy,
			String sortOrder, String keyword) throws DataInvalidException;

	public void ignoreUrl(IgnoreChildUrlRequestDTO request) throws DataInvalidException;

	public ContentResponseDTO getContent(Long childId) throws DataInvalidException;

}
