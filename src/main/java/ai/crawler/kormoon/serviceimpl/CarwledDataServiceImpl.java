package ai.crawler.kormoon.serviceimpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.crawler.kormoon.entity.CrawledDataChildEntity;
import ai.crawler.kormoon.entity.CrawledDataEntity;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.repository.CrawledDataChildRepository;
import ai.crawler.kormoon.repository.CrawledDataRepository;
import ai.crawler.kormoon.service.CrawledDataService;

@Service
public class CarwledDataServiceImpl implements CrawledDataService {

	@Autowired
	CrawledDataRepository crawledDataRepository;

	@Autowired
	CrawledDataChildRepository crawledDataChildRepository;
	

	@Autowired
	ExecutorService executorService;

	@Override
	public void getPageLinks(String rootLink, Set<String> keywordSet, int maxDepthLevel, SettingsEntity ruleEntity) {
				System.out.println("keywordList:"+keywordSet);
				
		ArrayList<String> keywordList = new ArrayList<>(keywordSet)	;	
		CrawledDataEntity crawledDataEntity = new CrawledDataEntity();
		crawledDataEntity.setUrl(rootLink);
		crawledDataEntity.getKeywordList().addAll(ruleEntity.getKeywordList());
		crawledDataEntity.setRuleEntity(ruleEntity);
		crawledDataRepository.saveAndFlush(crawledDataEntity);
		
		Set<String> childIgnoreUrls = new HashSet<String>();	
		childIgnoreUrls = crawledDataChildRepository.findIgnoreUrlsBySettingsId(ruleEntity.getId());
		long countCrawledData = crawledDataRepository.countBySettingsId(ruleEntity.getId());
		
		try {
			Set<String> visitedUrlSet = new HashSet<>();
			visitedUrlSet.add(rootLink);
			visitedUrlSet.addAll(childIgnoreUrls);
			
			Future<CrawlerResponseModel> parentFutureResult = executorService.submit(new Crawler(rootLink, 0L, keywordList, visitedUrlSet));

			CrawledDataChildEntity rootCrawledDataChildEntity = new CrawledDataChildEntity();
			
			Optional<CrawledDataChildEntity> getParentEntityByUrl = null;
			if(!parentFutureResult.get().getURL().isEmpty()) {
				getParentEntityByUrl = crawledDataChildRepository.findByUrlLike(ruleEntity.getId(), parentFutureResult.get().getURL());
				if(getParentEntityByUrl.isPresent()) {
					rootCrawledDataChildEntity.setId(getParentEntityByUrl.get().getId());
				}	
			}
			rootCrawledDataChildEntity.setUrl(parentFutureResult.get().getURL());
			rootCrawledDataChildEntity.setContent(parentFutureResult.get().getContent());
//			rootCrawledDataChildEntity.setCrawledDataEntity(crawledDataEntity);
			rootCrawledDataChildEntity.setCrawledDataEntity(getParentEntityByUrl!=null && getParentEntityByUrl.isPresent() ?getParentEntityByUrl.get().getCrawledDataEntity():crawledDataEntity);
			rootCrawledDataChildEntity.setLevel(parentFutureResult.get().getDepth());
			rootCrawledDataChildEntity.setIsFetched(0L);
			rootCrawledDataChildEntity.setKeywordList(parentFutureResult.get().getKeywords());
			
			Set<String> currentChildUrlSet = parentFutureResult.get().getCurrentChildUrlSet();
			
			crawledDataChildRepository.save(rootCrawledDataChildEntity);
		
			if (maxDepthLevel > 0) {

				long currentDepthLevel = 1;
				List<Future<CrawlerResponseModel>> futureResultList = new ArrayList<>();
				
				try {
					
					while(currentDepthLevel <= maxDepthLevel) {
						
						for(String url : currentChildUrlSet) {
							futureResultList.add(executorService.submit(new Crawler(url, currentDepthLevel, keywordList, visitedUrlSet)));
						}
						
						System.out.println("maxDepthLevel :"+maxDepthLevel);
						System.out.println("currentDepthLevel:"+currentDepthLevel);
						System.out.println("No of urls in this level :"+ currentChildUrlSet.size());
						System.out.println("No of urls visited :"+ visitedUrlSet.size());

						currentChildUrlSet.clear();
						
						for (Future<CrawlerResponseModel> future : futureResultList) {							
							try {
								CrawlerResponseModel responseModel = future.get();
								if (responseModel != null && responseModel.getContent() !=  null && !responseModel.getContent().isEmpty()) {

									CrawledDataChildEntity crawledDataChildEntity = new CrawledDataChildEntity();
									Optional<CrawledDataChildEntity> getChildEntityByUrl = null;
									if(countCrawledData>0) {
										getChildEntityByUrl = crawledDataChildRepository.findByUrlLike(ruleEntity.getId(), future.get().getURL());
										if(getChildEntityByUrl.isPresent()) {
											crawledDataChildEntity.setId(getChildEntityByUrl.get().getId());
										}
									}
									
									crawledDataChildEntity.setCrawledDataEntity(getChildEntityByUrl.isPresent()&&getChildEntityByUrl!=null ? getChildEntityByUrl.get().getCrawledDataEntity():crawledDataEntity);
									crawledDataChildEntity.setUrl(future.get().getURL());
									crawledDataChildEntity.setContent(future.get().getContent());
									crawledDataChildEntity.setLevel(future.get().getDepth());
									crawledDataChildEntity.setIsFetched(0L);
									crawledDataChildEntity.setKeywordList(future.get().getKeywords());
									
									crawledDataChildRepository.save(crawledDataChildEntity);
									
									if(future.get().getCurrentChildUrlSet()!=null) {
										currentChildUrlSet.addAll(future.get().getCurrentChildUrlSet());
									}
								}
								
							} catch (InterruptedException e) {
								e.printStackTrace();
							} catch (Exception e) {
								Throwable t = e.getCause();
								System.err.println("Uncaught exception is detected! " + t + " st: "
										+ Arrays.toString(t.getStackTrace()));
							}

						}

						System.out.println("currentDepthLevel : "+currentDepthLevel+" is done.");

						futureResultList.clear();
						currentDepthLevel++ ;
						System.out.println("Next Depth Level :"+ currentDepthLevel);
					}
			        
				} catch(Exception e) {
					System.err.println("For '" + rootLink + "': " + e.getMessage());
					e.printStackTrace();
				}
				
			}
			
			rootCrawledDataChildEntity.setIsFetched(1L);
			crawledDataChildRepository.save(rootCrawledDataChildEntity);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			Throwable t = e.getCause();
			System.err.println("Uncaught exception is detected! " + t + " st: "
					+ Arrays.toString(t.getStackTrace()));
		}
		
	}

}
