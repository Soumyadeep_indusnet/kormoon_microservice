package ai.crawler.kormoon.serviceimpl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.ContentHandler;

import ai.crawler.kormoon.utils.SSLHelper;
import ai.crawler.kormoon.utils.Utils;

public class Crawler implements Callable<CrawlerResponseModel>{
	
	private String urlLink;
	private Long currentDepthLevel;
	private List<String> keywords;
	private Set<String> visitedUrlSet;
	private Set<String> childUrlSet = new HashSet<>();
	private String errorMessage = "";

	public Crawler(String urlLink, Long currentDepthLevel, List<String> keywords, Set<String> visitedUrlSet) {
		this.urlLink = urlLink;
		this.currentDepthLevel = currentDepthLevel;
		this.keywords = keywords;
		this.visitedUrlSet = visitedUrlSet;

	}

	
	public synchronized CrawlerResponseModel parseHTML(String url, Long currentDepthLevel) {
		Map<String, Set<String>> mapContentKeywords = new HashMap<String, Set<String>>();
		
		if(url.contains("https")|| url.contains("http")){
		if(url.contains(".pdf") || url.contains(".doc") || url.contains(".docx") || url.contains(".xht") || url.contains(".xls")) {
			mapContentKeywords = getContentFromDocument(url, keywords);
		}else{
			try {
				mapContentKeywords = filteredcontent(url, keywords);
				childUrlSet.addAll(getAllUrl(url, visitedUrlSet));
			} catch (Exception e) {
				errorMessage = e.getMessage();
				System.out.println("ERROR" + e);
			}			
		}
		}
		else {
			errorMessage = "Not a valid url";
			System.out.println("Not a valid url");
		}
		 
		if(mapContentKeywords!=null && !mapContentKeywords.isEmpty()) {
			Entry<String, Set<String>> entry = mapContentKeywords.entrySet().iterator().next();
			return new CrawlerResponseModel(url, currentDepthLevel, entry.getKey(), entry.getValue() , errorMessage, childUrlSet);

		}else {
			return new CrawlerResponseModel(url, currentDepthLevel, "" , new HashSet<>() , errorMessage, childUrlSet);

		}
	}
	
	
	public synchronized Map<String, Set<String>> filteredcontent(String Url,List<String> keywords) throws IOException {

		Document document = SSLHelper.getConnection(Url)
				.ignoreContentType(true)
				.ignoreHttpErrors(true)
				.timeout(50000)
				.get();
		
		if(Url.contains("legislation.gov.uk")) {
			Element body = document.select("div[class=LegSnippet]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:02016R0679-20160504")) {
			Element body = document.select("div[class=tabContent]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("mrc.ukri.org")) {
			Element body = document.select("div[class=wrap]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://www.lexology.com/library/detail.aspx?g=29656ac8-d3f0-4770-bbcc-2a3fb79506a8")) {
			Element body = document.select("div[class=gated-content]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://www.huntonprivacyblog.com/2015/02/09/article-29-working-party-clarifies-scope-health-data-processed-lifestyle-wellbeing-apps/")) {
			Element body = document.select("div#lxb_af-loop").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://www.lw.com/thoughtLeadership/ClinicaltrialsundertheGDPR-Whatshouldsponsorsconsider")) {
			return getContentFromDocument(Url, keywords);
		}else if(Url.contains("https://edpb.europa.eu/our-work-tools/our-documents/opinion-art-70/opinion-32019-concerning-questions-and-answers-interplay_en")) {
			Element body = document.select("div[class=row]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://digital.nhs.uk/data-and-information/looking-after-information/data-security-and-information-governance/codes-of-practice-for-handling-information-in-health-and-care")) {
			Element body = document.select("div[class=grid-row]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://ico.org.uk/for-organisations/guide-to-data-protection/guide-to-the-general-data-protection-regulation-gdpr/consent/")) {
			Element body = document.select("div[class=row]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://digital.nhs.uk/services/national-data-opt-out")) {
			Element body = document.select("article[class=article article--service]").first();
			return getContentFromUrl(Url, keywords,body);
		}else if(Url.contains("https://www.hra.nhs.uk/planning-and-improving-research/policies-standards-legislation/data-protection-and-information-governance/")) {
			Element body = document.select("article[class=article]").first();
			return getContentFromUrl(Url, keywords,body);
		}else {
			Element body = document.select("body").first();
			return getContentFromUrl(Url, keywords,body);
		}
	}
	
	public synchronized Map<String, Set<String>> getContentFromDocument(String Url, List<String> keywords) {
		 
		Map<String, Set<String>> mapContentKeywords = new HashMap<String, Set<String>>();
		 
		 try {
				URL documentUrl = new URL(Url);
		        ContentHandler contenthandler = new BodyContentHandler(-1);
		        Metadata metadata = new Metadata();
		        InputStream input = TikaInputStream.get(documentUrl, metadata);
		        Detector detector = new DefaultDetector();
		        Parser  parser = new AutoDetectParser(detector);
		        parser.parse(input, contenthandler, metadata, new ParseContext()); 
		        
		        String PageContent = contenthandler.toString();
		        
				if(!PageContent.isEmpty() && keywords!=null && keywords.size()>0) {
					 if(!getMatchKeywords(keywords, PageContent).isEmpty()) {
						 mapContentKeywords.put(PageContent, getMatchKeywords(keywords, PageContent));
					 }
				}
				
			}catch (Exception e) {
				errorMessage = e.getMessage();
				System.err.println("For Document : " + Url + "': " + e.getMessage());
			}
			return mapContentKeywords;
		}
		
		public synchronized Map<String, Set<String>> getContentFromUrl(String URL, List<String> keywords,Element body) throws IOException {
			
			String PageContent = "";
			Map<String, Set<String>> mapContentKeywords = new HashMap<String, Set<String>>();

			if(body!=null) {
				Elements children = body.children();
				if(children!=null && !children.isEmpty()) {
					for(Element elem : children) {
						String innerHtmlText = elem.text();
						String[] innerTextArr = {};
						if(!innerHtmlText.trim().equals("") && innerHtmlText.contains("|")) {
							innerTextArr = innerHtmlText.split("\\|");
						}
						List<String> innerTextList = Arrays.asList(innerTextArr);
						innerTextList = new ArrayList<>(innerTextList);
						//System.out.println("innerTextArr.length-->"+innerTextList.size());
						if(innerTextList.size() < 1) {
							innerTextList.add(innerHtmlText);
						}
						//System.out.println(innerTextList.toString());
						if(!innerTextList.isEmpty()) {
							for(String innerText : innerTextList) {
								if(innerText.trim().isEmpty()) {
									continue;
								}
								PageContent += innerText+"\n";
							}
						}
					}
				}
			}
			if(keywords!=null && keywords.size()>0) {
				if(!getMatchKeywords(keywords, PageContent).isEmpty()) {
					mapContentKeywords.put(PageContent, getMatchKeywords(keywords, PageContent));
				}
			}

			return mapContentKeywords;
		}
	
		
		public synchronized Set<String> getAllUrl(String url, Set<String> visitedUrlSet) throws Exception {

			Set<String> urlSet = new HashSet<>();

			try {
				
				Document document = SSLHelper.getConnection(url).ignoreContentType(true).ignoreHttpErrors(true).timeout(50000)
						.get();

				Elements linksOnPage = document.select("a[href]");

				for (Element page : linksOnPage) {
					String innerUrl = Utils.getAbsoluteURLWithoutInternalReference( page.attr("abs:href") );
					if (Utils.isValidSameDomainURL(url, innerUrl) && !visitedUrlSet.contains(innerUrl)) {
						urlSet.add(innerUrl);
					}
				}
				
			} catch(Exception e) {
				
				e.printStackTrace();
				
			}
			
			visitedUrlSet.addAll(urlSet);

			return urlSet;
		}

		
		public synchronized Set<String> getMatchKeywords(List<String> keywordList, String textContent){ 
//			String inputText = textContent.toLowerCase().replaceAll("\\s", "");
			Set<String> childKeywordSet = new HashSet<>();
	        for (String keyword : keywordList) {
	            Pattern p = Pattern.compile(keyword);
	            Matcher m = p.matcher(textContent);
	            if (m.find()) {
	            	childKeywordSet.add(keyword);
	            }
	        }
	        
			return childKeywordSet;
		}


		@Override
		public CrawlerResponseModel call() throws Exception {
			CrawlerResponseModel crawlerResponseModel = null;
			
			try {
				crawlerResponseModel = parseHTML(urlLink, currentDepthLevel);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
//			CrawlerResponseModel crawlerResponseModel = crawl(urlLink, MaxDepthLevel);
			return crawlerResponseModel;
		}

}
