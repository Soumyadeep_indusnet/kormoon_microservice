package ai.crawler.kormoon.serviceimpl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import ai.crawler.kormoon.dto.request.CrawlerCreateRequestDTO;
import ai.crawler.kormoon.dto.response.BasicIdAndNameResponseDTO;
import ai.crawler.kormoon.dto.response.SectorResponseDTO;
import ai.crawler.kormoon.dto.response.SectorResponseDTO.Sector;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.entity.SectorEntity;
import ai.crawler.kormoon.enums.FrequencyType;
import ai.crawler.kormoon.enums.SettingsStateType;
import ai.crawler.kormoon.enums.SettingsStatusType;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.repository.CountryRepository;
import ai.crawler.kormoon.repository.SettingsRepository;
import ai.crawler.kormoon.repository.SectorRepository;
import ai.crawler.kormoon.repository.SourceCategoryRepository;
import ai.crawler.kormoon.repository.SourceSubCategoryRepository;
import ai.crawler.kormoon.service.CrawlerCreateService;

@Service
public class CrawlerCreateServiceImpl implements CrawlerCreateService {

	@Autowired
	SettingsRepository ruleRepository;

	@Autowired
	SectorRepository sectorRepository;

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	SourceCategoryRepository sourceCategoryRepository;

	@Autowired
	SourceSubCategoryRepository sourceSubCategoryRepository;

	@Autowired
	MessageSource messageSource;

	@Override
	public List<BasicIdAndNameResponseDTO> getAllCountriesList() {

		return countryRepository.getAllCountriesListOrderByNameAsc();

	}

	@Override
	public List<BasicIdAndNameResponseDTO> getAllCategoriesList() {

		return sourceCategoryRepository.getAllCategoriesListOrderByNameAsc();

	}

	@Override
	public List<BasicIdAndNameResponseDTO> getAllSubCategoriesListByCategoryId(Long categoryId) {

		return sourceSubCategoryRepository.getAllSubCategoriesListByCategoryIdOrderByNameAsc(categoryId);

	}

	@Override
	public List<SectorResponseDTO> getAllSectors() {

		List<SectorEntity> sectorsList = sectorRepository.findAllByOrderByNameAsc();

		List<SectorResponseDTO> responseList = new ArrayList<>();

		List<SectorEntity> levelZeroSectorsList = sectorsList.stream().filter(sector -> sector.getLevel() == 0)
				.collect(Collectors.toList());

		List<SectorEntity> levelOneSectorsList = sectorsList.stream().filter(sector -> sector.getLevel() == 1)
				.collect(Collectors.toList());

		List<SectorEntity> levelTwoSectorsList = sectorsList.stream().filter(sector -> sector.getLevel() == 2)
				.collect(Collectors.toList());

		for (SectorEntity levelZeroSector : levelZeroSectorsList) {

			SectorResponseDTO response = new SectorResponseDTO();

			Sector sector = new Sector();

			sector.setId(levelZeroSector.getId());
			sector.setName(levelZeroSector.getName());

			List<SectorResponseDTO> levelOneChildSectorsResponseList = new ArrayList<>();

			List<SectorEntity> levelOneChildSectorsEntityList = levelOneSectorsList.stream()
					.filter(levelOneChildSector -> levelOneChildSector.getParentSector().equals(levelZeroSector))
					.collect(Collectors.toList());

			for (SectorEntity levelOneChildSector : levelOneChildSectorsEntityList) {

				SectorResponseDTO levelOneChildSectorResponse = new SectorResponseDTO();

				Sector levelOneSector = new Sector();

				levelOneSector.setId(levelOneChildSector.getId());
				levelOneSector.setName(levelOneChildSector.getName());

				List<SectorResponseDTO> levelTwoChildSectorsResponseList = new ArrayList<>();

				List<SectorEntity> levelTwoChildSectorsEntityList = levelTwoSectorsList.stream().filter(
						levelTwoChildSector -> levelTwoChildSector.getParentSector().equals(levelOneChildSector))
						.collect(Collectors.toList());

				for (SectorEntity levelTwoChildSector : levelTwoChildSectorsEntityList) {

					SectorResponseDTO levelTwoChildSectorResponse = new SectorResponseDTO();

					Sector levelTwoSector = new Sector();

					levelTwoSector.setId(levelTwoChildSector.getId());
					levelTwoSector.setName(levelTwoChildSector.getName());

					levelTwoChildSectorResponse.setSector(levelTwoSector);

					levelTwoChildSectorsResponseList.add(levelTwoChildSectorResponse);

				}

				levelOneSector.setChildSectorsList(levelTwoChildSectorsResponseList);

				levelOneChildSectorResponse.setSector(levelOneSector);

				levelOneChildSectorsResponseList.add(levelOneChildSectorResponse);

			}

			sector.setChildSectorsList(levelOneChildSectorsResponseList);

			response.setSector(sector);

			responseList.add(response);

		}

		return responseList;
	}

	@Override
	public void save(CrawlerCreateRequestDTO request) throws DataInvalidException {

		if (request.getStartsFrom().before(new DateTime().now().withTimeAtStartOfDay().toDate())) {

			throw new DataInvalidException(messageSource.getMessage("invalid.start.date",
					new String[] { DateTimeFormatter.ofPattern("E, MMM dd yyyy").format(LocalDate.now()) }, null));

		}

		if (request.getKeywordList().stream().anyMatch(keyword -> keyword.trim().isEmpty())) {

			throw new DataInvalidException(messageSource.getMessage("invalid.keyword", null, null));

		}

		request.setName(request.getName().trim().replaceAll("(  )+", " "));

		if (ruleRepository.existsByName(request.getName())) {

			throw new DataInvalidException(messageSource.getMessage("crawler.already.exists", null, null));

		}

		SettingsEntity crawler = new SettingsEntity();

		crawler.setName(request.getName());
		crawler.setUrl(request.getUrl());

		crawler.setKeywordList(request.getKeywordList());

		crawler.setMaxDepthLevel(request.getMaxDepthLevel());
		crawler.setStartsFrom(request.getStartsFrom());

		crawler.setFrequency(request.getFrequency().equalsIgnoreCase("DAILY") ? FrequencyType.DAILY
				: request.getFrequency().equalsIgnoreCase("WEEKLY") ? FrequencyType.WEEKLY : FrequencyType.BI_WEEKLY);

		crawler.setIsSecuredURL(request.getIsSecuredURL());

		if (request.getIsSecuredURL()) {

			if ((request.getLoginUsername() == null || request.getLoginUsername().trim().isEmpty())
					|| (request.getLoginPassword() == null || request.getLoginPassword().trim().isEmpty())) {

				throw new DataInvalidException(messageSource.getMessage("invalid.username.password", null, null));

			}

			crawler.setLoginUserName(request.getLoginUsername());
			crawler.setLoginPassword(request.getLoginPassword());

		}

		crawler.setFailureAlert(request.getFailureAlert());

		crawler.setStatus(
				request.getStatus().equalsIgnoreCase("ACTIVE") ? SettingsStatusType.ACTIVE : SettingsStatusType.INACTIVE);

		crawler.setState(SettingsStateType.NOT_YET_STARTED);

		crawler.setSelectedCountriesList(countryRepository.findAllByIdIn(request.getSelectedCountryIdList()));

		crawler.setSector(sectorRepository.findById(request.getSectorId()).orElseThrow(DataInvalidException::new));
		crawler.setSourceSubCategory(sourceSubCategoryRepository.findById(request.getSourceSubCategoryId())
				.orElseThrow(DataInvalidException::new));

		ruleRepository.save(crawler);

	}

}
