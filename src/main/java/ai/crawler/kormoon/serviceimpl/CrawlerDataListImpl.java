package ai.crawler.kormoon.serviceimpl;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ai.crawler.kormoon.dto.request.CrawlerCreateRequestDTO;
import ai.crawler.kormoon.dto.response.CrawlerListResponse;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.dto.response.SettingIdAndLastRunOnCustomProjection;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.enums.FrequencyType;
import ai.crawler.kormoon.enums.SettingsStateType;
import ai.crawler.kormoon.enums.SettingsStatusType;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.model.mapper.WebCrawlerMapper;
import ai.crawler.kormoon.repository.CountryRepository;
import ai.crawler.kormoon.repository.CrawledDataRepository;
import ai.crawler.kormoon.repository.SectorRepository;
import ai.crawler.kormoon.repository.SettingsRepository;
import ai.crawler.kormoon.repository.SourceSubCategoryRepository;
import ai.crawler.kormoon.service.CrawlerListService;

@Service
public class CrawlerDataListImpl implements CrawlerListService {

	@Autowired
	SourceSubCategoryRepository sourceSubCategoryRepository;

	@Autowired
	SectorRepository sectorRepository;

	@Autowired
	CountryRepository countryRepository;

	@Autowired
	SettingsRepository ruleRepository;

	@Autowired
	MessageSource message;

	@Autowired
	MessageSource messageSource;

	@Autowired
	CrawledDataRepository crawledDataRepository;

	@Transactional(readOnly = true)
	@Override
	public ListResponse<CrawlerListResponse> findCrawlerDataList(int pageNo, int pageSize, String searchByNameFrequency,
			String searchByStatus, String sortBy, String sortOrder) throws DataInvalidException {

		switch (sortBy) {

		case "":
			sortBy = "";
			break;

		case "name":
			sortBy = "name";
			break;

		case "failureAlert":
			sortBy = "failureAlert";
			break;

		case "frequency":
			sortBy = "frequency";
			break;

		case "status":
			sortBy = "status";
			break;

		case "createdDate":
			sortBy = "createdDate";
			break;

		default:
			throw new DataInvalidException();

		}

		Pageable pageable;
		if (!sortBy.isEmpty()) {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by(Direction.fromString(sortOrder), sortBy));
		} else {
			pageable = PageRequest.of(pageNo, pageSize, Sort.by("id").descending());
		}

		Page<SettingsEntity> crwalerList = null;
		if (!searchByStatus.isEmpty()) {
			crwalerList = ruleRepository.findByCrawlerStatus(searchByNameFrequency, searchByStatus, pageable);
		} else {
			crwalerList = ruleRepository.findByCrawlerName(searchByNameFrequency, searchByStatus, pageable);
		}

		ListResponse<CrawlerListResponse> response = new ListResponse<>();
		response.setTotal(crwalerList.getTotalElements());

		Set<SettingIdAndLastRunOnCustomProjection> lastRunOnDateSet = crawledDataRepository.getLastRunOnSet();

		for (SettingsEntity crawler : crwalerList) {

			Date lastRunOn = new Date();
			Date nextScheduledOn = new Date();

			if (lastRunOnDateSet.stream().anyMatch(lastRun -> lastRun.getSettingsId().equals(crawler.getId()))) {

				lastRunOn = lastRunOnDateSet.stream().filter(lastRun -> lastRun.getSettingsId().equals(crawler.getId()))
						.findAny().get().getLastRunOn();

			} else {

				lastRunOn = new Date();

			}

			if (crawler.getFrequency().equals(FrequencyType.DAILY)) {

				nextScheduledOn = new DateTime(lastRunOn).plusDays(1).toDate();

			} else if (crawler.getFrequency().equals(FrequencyType.BI_WEEKLY)) {

				nextScheduledOn = new DateTime(lastRunOn).plusDays(3).toDate();

			} else {

				nextScheduledOn = new DateTime(lastRunOn).plusDays(7).toDate();

			}

			response.getEntries().add(WebCrawlerMapper.toCrawlerListResponse(crawler, lastRunOn, nextScheduledOn));

		}
		return response;

	}

	@Override
	public Optional<SettingsEntity> getAllListByCrawlerId(Long crawlerId) throws DataInvalidException {
		Optional<SettingsEntity> crwalerList = ruleRepository.findById(crawlerId);

		return crwalerList;
	}

	@Override
	public void updateCrawlerById(Long id, CrawlerCreateRequestDTO crawlerCreateRequestDTO)
			throws DataInvalidException {

		if (!ruleRepository.findById(id).isPresent()) {
			throw new DataInvalidException(messageSource.getMessage("invalid.id", null, null));
		}

		SettingsEntity existingCrawler = ruleRepository.findById(id).get();

		existingCrawler.setName(crawlerCreateRequestDTO.getName());
		existingCrawler.setUrl(crawlerCreateRequestDTO.getUrl());

		existingCrawler.setKeywordList(crawlerCreateRequestDTO.getKeywordList());

		existingCrawler.setMaxDepthLevel(crawlerCreateRequestDTO.getMaxDepthLevel());
		existingCrawler.setStartsFrom(crawlerCreateRequestDTO.getStartsFrom());

		existingCrawler
				.setFrequency(crawlerCreateRequestDTO.getFrequency().equalsIgnoreCase("DAILY") ? FrequencyType.DAILY
						: crawlerCreateRequestDTO.getFrequency().equalsIgnoreCase("WEEKLY") ? FrequencyType.WEEKLY
								: FrequencyType.BI_WEEKLY);

		existingCrawler.setIsSecuredURL(crawlerCreateRequestDTO.getIsSecuredURL());

		existingCrawler.setFailureAlert(crawlerCreateRequestDTO.getFailureAlert());

		existingCrawler
				.setStatus(crawlerCreateRequestDTO.getStatus().equalsIgnoreCase("ACTIVE") ? SettingsStatusType.ACTIVE
						: SettingsStatusType.INACTIVE);

		existingCrawler.setState(SettingsStateType.NOT_YET_STARTED);

		existingCrawler.setSelectedCountriesList(
				countryRepository.findAllByIdIn(crawlerCreateRequestDTO.getSelectedCountryIdList()));

		existingCrawler.setSector(sectorRepository.findById(crawlerCreateRequestDTO.getSectorId())
				.orElseThrow(DataInvalidException::new));
		existingCrawler.setSourceSubCategory(sourceSubCategoryRepository
				.findById(crawlerCreateRequestDTO.getSourceSubCategoryId()).orElseThrow(DataInvalidException::new));

		ruleRepository.save(existingCrawler);

	}

}
