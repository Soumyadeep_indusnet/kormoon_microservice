package ai.crawler.kormoon.serviceimpl;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Builder
@Getter
@Setter
public class CrawlerResponseModel {
    private String URL;
    private Long depth;
    private String Content;
    private Set<String> keywords;
    private String errorMessage;
    private Set<String> currentChildUrlSet;
    
}
