package ai.crawler.kormoon.serviceimpl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import ai.crawler.kormoon.dto.request.ContentRequestDTO;
import ai.crawler.kormoon.dto.request.IgnoreChildUrlRequestDTO;
import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawledUrlsResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerReviewResponseDTO;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.entity.CrawledDataChildEntity;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.repository.CrawledDataChildRepository;
import ai.crawler.kormoon.repository.SettingsRepository;
import ai.crawler.kormoon.service.CrawlerReviewService;

@Service
public class CrawlerReviewServiceImpl implements CrawlerReviewService {

	@Autowired
	SettingsRepository settingsRepository;

	@Autowired
	CrawledDataChildRepository crawledDataChildRepository;

	@Override
	@Transactional(rollbackOn = Exception.class)
	public CrawlerReviewResponseDTO getCrawledUrls(Long settingsId, Integer pageNo, Integer limit, String sortBy,
			String sortOrder) throws DataInvalidException {

		SettingsEntity setting = settingsRepository.findById(settingsId).orElseThrow(DataInvalidException::new);

		CrawlerReviewResponseDTO response = new CrawlerReviewResponseDTO();

		response.setCrawlerName(setting.getName());
		response.setUrl(setting.getUrl());
		response.setKeywords(setting.getKeywordList());

		switch (sortBy) {

		case "":
			sortBy = "";
			break;

		case "content":
			break;

		case "url":
			break;

		case "createdDate":
			break;

		default:
			throw new DataInvalidException();

		}

		ListResponse<CrawledUrlsResponseDTO> crawledUrlsResponseList = new ListResponse<>();

		Page<CrawledDataChildEntity> crawledUrls = crawledDataChildRepository.findAllBySettingsId(settingsId,
				PageRequest.of(pageNo, limit, Sort.by(Direction.fromString(sortOrder), sortBy)));

		crawledUrlsResponseList.setTotal(crawledUrls.getTotalElements());

		for (CrawledDataChildEntity crawledUrl : crawledUrls) {

			CrawledUrlsResponseDTO childUrlResponse = new CrawledUrlsResponseDTO();

			childUrlResponse.setId(crawledUrl.getId());
			childUrlResponse.setKeywords(crawledUrl.getKeywordList());
			childUrlResponse.setUrl(crawledUrl.getUrl());
			childUrlResponse
					.setContent(crawledUrl.getContent().length() > 100 ? crawledUrl.getContent().substring(0, 100)
							: crawledUrl.getContent());
			childUrlResponse.setIsIgnored(crawledUrl.getIsIgnored());

			crawledUrlsResponseList.getEntries().add(childUrlResponse);

		}

		response.setCrawledUrls(crawledUrlsResponseList);

		return response;
	}

	@Override
	public void ignoreUrl(IgnoreChildUrlRequestDTO request) throws DataInvalidException {

		CrawledDataChildEntity crawledDataChild = crawledDataChildRepository.findById(request.getChildId())
				.orElseThrow(DataInvalidException::new);

		crawledDataChild.setIsIgnored(request.getIsIgnored());

		crawledDataChildRepository.save(crawledDataChild);

	}

	@Override
	public ContentResponseDTO getContent(Long childId) throws DataInvalidException {

		return crawledDataChildRepository.getContentByChildId(childId);

	}

	@Override
	public void editContent(ContentRequestDTO request) throws DataInvalidException {

		CrawledDataChildEntity crawledDataChild = crawledDataChildRepository.findById(request.getChildId())
				.orElseThrow(DataInvalidException::new);

		crawledDataChild.setContent(request.getContent());

		crawledDataChildRepository.save(crawledDataChild);

	}

}
