package ai.crawler.kormoon.serviceimpl;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import ai.crawler.kormoon.dto.request.IgnoreChildUrlRequestDTO;
import ai.crawler.kormoon.dto.response.ContentResponseDTO;
import ai.crawler.kormoon.dto.response.CrawledUrlSearchCustomProjection;
import ai.crawler.kormoon.dto.response.CrawledUrlsResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerReviewResponseDTO;
import ai.crawler.kormoon.dto.response.CrawlerSearchListCustomProjection;
import ai.crawler.kormoon.dto.response.CrawlerSearchListResponseDTO;
import ai.crawler.kormoon.dto.response.ListResponse;
import ai.crawler.kormoon.entity.CrawledDataChildEntity;
import ai.crawler.kormoon.entity.SettingsEntity;
import ai.crawler.kormoon.enums.SettingsStatusType;
import ai.crawler.kormoon.exception.DataInvalidException;
import ai.crawler.kormoon.repository.CrawledDataChildRepository;
import ai.crawler.kormoon.repository.SettingsRepository;
import ai.crawler.kormoon.service.CrawlerSearchService;

@Service
public class CrawlerSearchServiceImpl implements CrawlerSearchService {

	@Autowired
	SettingsRepository settingsRepository;

	@Autowired
	CrawledDataChildRepository crawledDataChildRepository;

	@Override
	public ListResponse<CrawlerSearchListResponseDTO> getCrawlerList(Integer pageNo, Integer pageSize, String keyword,
			String sortBy, String sortOrder) throws DataInvalidException {

		switch (sortBy) {

		case "":
			sortBy = "";
			break;

		case "name":
			break;

		case "url":
			break;

		case "createdDate":
			break;

		default:
			throw new DataInvalidException();

		}

		ListResponse<CrawlerSearchListResponseDTO> responseList = new ListResponse<>();

		if (keyword.isEmpty()) {

			Page<SettingsEntity> settingsList = settingsRepository.findAllByStatus(SettingsStatusType.ACTIVE,
					PageRequest.of(pageNo, pageSize, Sort.by(Direction.fromString(sortOrder), sortBy)));

			responseList.setTotal(settingsList.getTotalElements());

			for (SettingsEntity setting : settingsList) {

				CrawlerSearchListResponseDTO response = new CrawlerSearchListResponseDTO();

				response.setId(setting.getId());
				response.setName(setting.getName());
				response.setUrl(setting.getUrl());
				response.setKeywordList(setting.getKeywordList());

				responseList.getEntries().add(response);

			}

		} else {

			Page<CrawlerSearchListCustomProjection> settingsList = settingsRepository.getCrawlerList(keyword,
					SettingsStatusType.ACTIVE,
					PageRequest.of(pageNo, pageSize, Sort.by(Direction.fromString(sortOrder), sortBy)));

			responseList.setTotal(settingsList.getTotalElements());

			for (CrawlerSearchListCustomProjection setting : settingsList) {

				CrawlerSearchListResponseDTO response = new CrawlerSearchListResponseDTO();

				response.setId(setting.getId());
				response.setName(setting.getName());
				response.setUrl(setting.getUrl());
				response.setKeywordList(new HashSet<String>(Arrays.asList(setting.getKeyword())));

				responseList.getEntries().add(response);

			}

		}

		return responseList;
	}

	@Override
	public CrawlerReviewResponseDTO getCrawledUrls(Long settingsId, Integer pageNo, Integer pageSize, String sortBy,
			String sortOrder, String keyword) throws DataInvalidException {

		CrawlerReviewResponseDTO response = new CrawlerReviewResponseDTO();

		switch (sortBy) {

		case "":
			sortBy = "";
			break;

		case "content":
			break;

		case "url":
			break;

		case "createdDate":
			break;

		default:
			throw new DataInvalidException();

		}

		SettingsEntity setting = settingsRepository.findById(settingsId).orElseThrow(DataInvalidException::new);

		response.setCrawlerName(setting.getName());
		response.setUrl(setting.getUrl());

		if (keyword.isEmpty()) {

			response.setKeywords(setting.getKeywordList());

			Page<CrawledDataChildEntity> childList = crawledDataChildRepository.findAllBySettingsId(settingsId,
					PageRequest.of(pageNo, pageSize, Sort.by(Direction.fromString(sortOrder), sortBy)));

			response.getCrawledUrls().setTotal(childList.getTotalElements());

			for (CrawledDataChildEntity child : childList) {

				CrawledUrlsResponseDTO childResponse = new CrawledUrlsResponseDTO();

				childResponse.setId(child.getId());
				childResponse.setKeywords(child.getKeywordList());
				childResponse.setUrl(child.getUrl());
				childResponse.setIsIgnored(child.getIsIgnored());
				childResponse.setContent(
						child.getContent().length() > 100 ? child.getContent().substring(0, 100) : child.getContent());

				response.getCrawledUrls().getEntries().add(childResponse);

			}

		} else {

			Page<CrawledUrlSearchCustomProjection> childList = crawledDataChildRepository.findAllBySettingsIdAndKeyword(
					keyword, settingsId,
					PageRequest.of(pageNo, pageSize, Sort.by(Direction.fromString(sortOrder), sortBy)));

			response.setKeywords(new HashSet<String>(
					Arrays.asList(childList.isEmpty() ? keyword : childList.getContent().get(0).getKeyword())));

			response.getCrawledUrls().setTotal(childList.getTotalElements());

			for (CrawledUrlSearchCustomProjection child : childList) {

				CrawledUrlsResponseDTO childResponse = new CrawledUrlsResponseDTO();

				childResponse.setId(child.getId());
				childResponse.setKeywords(new HashSet<String>(Arrays.asList(child.getKeyword())));
				childResponse.setUrl(child.getUrl());
				childResponse.setIsIgnored(child.getIsIgnored());
				childResponse.setContent(
						child.getContent().length() > 100 ? child.getContent().substring(0, 100) : child.getContent());

				response.getCrawledUrls().getEntries().add(childResponse);

			}

		}

		return response;
	}

	@Override
	public void ignoreUrl(IgnoreChildUrlRequestDTO request) throws DataInvalidException {

		CrawledDataChildEntity crawledDataChild = crawledDataChildRepository.findById(request.getChildId())
				.orElseThrow(DataInvalidException::new);

		crawledDataChild.setIsIgnored(request.getIsIgnored());

		crawledDataChildRepository.save(crawledDataChild);

	}

	@Override
	public ContentResponseDTO getContent(Long childId) throws DataInvalidException {

		return crawledDataChildRepository.getContentByChildId(childId);

	}

}
