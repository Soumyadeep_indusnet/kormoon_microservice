package ai.crawler.kormoon.serviceimpl;

import java.util.concurrent.Callable;

public class RootLinkCrawler implements Callable<RootLinksModel>{

	
	private String URL;
	private Long lastId;
	private String keyword;
	private int i;
	
	public RootLinkCrawler( String URL, String keyword, int i, Long lastId) {
		this.URL = URL;
		this.keyword = keyword;
		this.lastId = lastId;
		this.i = i;

	}
	
	@Override
	public RootLinksModel call() throws Exception {
		
		return new RootLinksModel(URL, keyword, i, lastId);
	}

}
