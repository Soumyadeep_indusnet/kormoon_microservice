package ai.crawler.kormoon.serviceimpl;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class RootLinksModel {
	private String URL;
	private String keyword;
	private int i; 
	private Long lastId;
    
}
