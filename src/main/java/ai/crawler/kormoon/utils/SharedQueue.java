package ai.crawler.kormoon.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.Lock;

public class SharedQueue {
	
	 static Queue<String> queue = new LinkedList<String>();
	 static HashMap<String, Integer> depthTable = new HashMap<String, Integer>();
	 List<String> Rootlinks;
	 String Url;
	 int depth;
	 Lock  lock;
	 
	public SharedQueue(String Url, int depth) {
	this.Url = Url;	
	this.depth = depth;	

	}
	
	public  synchronized static void SetRootLinks(List<String> Rootlinks, int Depth) {
		 // Add source URLs to uncrawled URL pool
        for (String s : Rootlinks) {
        	queue.add(s);
			depthTable.put(s, Depth);
        }
	}
	
	public  synchronized static void Set(String Url, int Depth) {
		queue.add(Url);
		depthTable.put(Url, Depth);
	 
	}
	
	public synchronized static  int GetDepth(String url) {
		
		if (depthTable.get(url) == null) {
	            return 0;
	        }
           int depth = depthTable.get(url);
           return depth;		
	}
	
	//Sync Coz it remove the item
	public synchronized static  String Get() {
		String Url=queue.poll();
		 return Url;
	}
	
	public synchronized static boolean IsEmpty() {
		if(queue.isEmpty()) {
			return true;
		}
		 return false;
	}
	
	public synchronized static boolean IsContains(String Url) {
		if(queue.contains(Url)) {
			return true;
		}
		 return false;
	}

	 public synchronized static int Size() {
		 int size=queue.size();
		 return size;
	 }

}
