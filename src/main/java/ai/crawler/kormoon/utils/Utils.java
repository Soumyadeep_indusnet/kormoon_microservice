package ai.crawler.kormoon.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.jsoup.Jsoup;

/**
 * @author Yi Hong, updated by Marco Perez
 * 
 *  You are free to make any changes to this
 *  class though you don't have to.
 *    
 */

public class Utils {
    
	/**
	 * 
	 * Strips HTML tags from a string
	 * 
	 * @param html
	 * @return string with all HTML tags removed
	 * 
	 */
	public static String getPlainText(String html){
		String text="";
		try{
			text= Jsoup.parse(html).text();
		}catch(Exception ex){
			return "";
		}
		return text;
	}
	
	
	public static String getFilterContent(List<String> keyWords,String text){ 
		 Map<String, Integer> map = new HashMap<String, Integer>();
		 String containt = "";
		 ArrayList<Integer> values = new ArrayList<Integer>();

		 String[] tokens = text.split("[\\s | , | \\.]");
		 
		 for(String kw:keyWords) {
			 map.put(kw, Long.valueOf(Stream.of(tokens).filter(w->w.equalsIgnoreCase(kw)).count()).intValue());
			 values = new ArrayList<Integer>(map.values());
		 }
		 
		 int sum = values.stream().reduce(0, (x,y) -> x+y);
		 if(sum > 0) {
			 containt = text;
		 }
			
		 return containt;
	}
	
    static String REPLACEABLE_EMPTY_STRING = "";
    static String URL_REMOVABLE_PROTOCOL_PATTERN = "http[s]?://";
    static String URL_REMOVABLE_URL_PATH_DELIMITER = "/";
    static String URL_REMOVABLE_URL_INTERNAL_LINK_DELIMITER = "#";
    static String URL_REMOVABLE_URL_PARAM_DELIMITER = "\\?";
    
	public static boolean isValidSameDomainURL(String currentURL, String nextURL) {
        String currentDomain = getAbsoluteURLWithoutInternalReference(currentURL)
                .replaceAll(URL_REMOVABLE_PROTOCOL_PATTERN,REPLACEABLE_EMPTY_STRING)
                .split(URL_REMOVABLE_URL_PATH_DELIMITER)[0];
        String nextDomain = getAbsoluteURLWithoutInternalReference(nextURL)
                .replaceAll(URL_REMOVABLE_PROTOCOL_PATTERN, REPLACEABLE_EMPTY_STRING)
                .split(URL_REMOVABLE_URL_PATH_DELIMITER)[0];
        return currentDomain.equalsIgnoreCase(nextDomain);
    }

    public static String getAbsoluteURLWithoutInternalReference(String urlString) {
        return getAbsoluteURLWithoutQueryParams(urlString.split(URL_REMOVABLE_URL_INTERNAL_LINK_DELIMITER)[0]);

    }

    static String getAbsoluteURLWithoutQueryParams(String urlString) {
        return urlString.split(URL_REMOVABLE_URL_PARAM_DELIMITER)[0];

    }
			
}


		
